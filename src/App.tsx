import logo from "./logo.svg";
import "./App.css";
import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Header from "./components/Header";
import HomePage from "./page/HomePage";
import ListingPage from "./page/ListingPage";
import RentingPage from "./page/RentingPage";
import AgentDiscoveryPage from "./page/AgentDiscoveryPage";
import ToolingPage from "./page/ToolingPage";
function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="body-page">
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/mua-nha">
              <ListingPage />
            </Route>
            <Route path="/thue-nha">
              <RentingPage />
            </Route>
            <Route path="/danh-sach-agent">
              <AgentDiscoveryPage />
            </Route>
            <Route path="/cong-cu">
              <ToolingPage />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
