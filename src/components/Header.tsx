import React from "react";
import styled from "styled-components";

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
const StyledLink = styled(Link)`
  padding: 5px;
  text-decoration: none;

`;
const StyledHeader = styled.header`
  display: flex;
  justify-content: center;
  width: 100vw;
`;
function Header() {
  return (
    <StyledHeader>
      <div>
        LOGO
      </div>
      <div>

      <StyledLink to="/">Trang Chủ</StyledLink>
      <StyledLink to="/mua-nha">Mua nhà</StyledLink>
      <StyledLink to="/thue-nha">Thuê nhà</StyledLink>
      <StyledLink to="/danh-sach-agent">Danh Sách Agent</StyledLink>
      <StyledLink to="/cong-cu">Công cụ Loan</StyledLink>
      </div>
      <div>
        <div>
          <div>Sign In</div>
          <div>Sign Up</div>
          <div>Save</div>
        </div>
        <div>Add Listing</div>
      </div>
    </StyledHeader>
  );
}

export default Header;
