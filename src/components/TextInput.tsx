import React from "react";
import styled from "styled-components";
const StyledInput = styled.input``;

type Props = {
  value: string | number;
};
function TextInput(props: Props) {
  return <input type="text" value={props.value} />;
}
export default styled(TextInput)`
  color: red;
`;
