export const setFont = {
  main: "font-family: 'Lato', sans-serif;",
}

export const setColor = {
  primaryColor: '',
  secondaryColor: '',
  coral: '#FF7F50',
  orangered: '#FF4500',
  mainWhite: '#fff',
  mainBlack: '#222',
  
  mainGrey: 'ececec',
  lightGrey: '#f7f7f7',
}
